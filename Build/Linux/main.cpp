#include "Polycode.h"
#include "PolycodeView.h"
#include "LDJAM33.h"

int main(int argc, char *argv[]) {
	PolycodeView *view = new PolycodeView("LDJAM33");
	LDJAM33 *app = new LDJAM33(view);
	while(app->Update()) {}
	return 0;
}
