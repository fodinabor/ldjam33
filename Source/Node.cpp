#include "Node.h"


Node::~Node() { }

bool Node::hasConnectionToNode(Node * node) {
	for (int i = 0; i < connected.size(); i++) {
		if (connected[i] == node) {
			return true;
		}
	}
	return false;
}

int Node::getConnectionIndex(Node * node) {
	for (int i = 0; i < connected.size(); i++) {
		if (connected[i] == node) {
			return i;
		}
	}
	return -1;
}

Node * Node::getRandomConnectedNode() {
	return connected[rand() % connected.size()];
}

SceneMesh* NodeNet::showConnections(Core *core, bool val) {
	if (!cons) {
		cons = new SceneMesh();
		cons->getMesh()->addSubmesh(MeshGeometry(MeshGeometry::LINE_MESH));
		//cons->getMesh()->useVertexColors = true;
		cons->setMaterial(core->getResourceManager()->getGlobalPool()->getMaterial("UnlitVertexColor"));

		Node* currentNode;
		for (int i = 0; i < nodes.size(); i++) {
			currentNode = nodes[i];
			for (int j = 0; j < currentNode->connected.size(); j++) {
				cons->getMesh()->getSubmeshPointer(0)->addVertex(currentNode->pos.x, currentNode->pos.y, 0.1);
				cons->getMesh()->getSubmeshPointer(0)->vertexColorArray.data.push_back(1.0);
				cons->getMesh()->getSubmeshPointer(0)->vertexColorArray.data.push_back(0);
				cons->getMesh()->getSubmeshPointer(0)->vertexColorArray.data.push_back(0);
				cons->getMesh()->getSubmeshPointer(0)->vertexColorArray.data.push_back(1.0);

				cons->getMesh()->getSubmeshPointer(0)->addVertex(currentNode->connected[j]->pos.x, currentNode->connected[j]->pos.y, 0.1);
				cons->getMesh()->getSubmeshPointer(0)->vertexColorArray.data.push_back(0);
				cons->getMesh()->getSubmeshPointer(0)->vertexColorArray.data.push_back(1.0);
				cons->getMesh()->getSubmeshPointer(0)->vertexColorArray.data.push_back(0);
				cons->getMesh()->getSubmeshPointer(0)->vertexColorArray.data.push_back(1.0);
			}
		}
	}
	showingConns = val;
	return cons;
}

bool NodeNet::getShowConnections() {
	return showingConns;
}

Node * NodeNet::findNearestNode(Vector2 pos) {
	Node *nearest = nodes[0];
	Number minDistance = pos.distance(nearest->pos);
	Number dist; 
	for (int i = 1; i < nodes.size(); i++) {
		dist = pos.distance(nodes[i]->pos);
		if (dist < minDistance) {
			minDistance = dist;
			nearest = nodes[i];
		}
		if (minDistance < 32)
			break;
	}

	return nearest;
}

stack<Node*> NodeNet::findPath(Node * fromNode, Node * toNode) {
	for (int i = 0; i < nodes.size(); i++) {
		nodes[i]->visited = false;
	}

	stack<Node*> path;
	Node* currentNode;
	Node* topNode;
	std::priority_queue<Node*, std::deque<Node*>, DereferenceCompareNode> priorityQueue[2];
	int pqi=0;
	Number tempNodeCost;
	
	fromNode->cost = 0;
	fromNode->visited = true;
	fromNode->distanceFromGoal = fromNode->mapPos.distance(toNode->mapPos);
	priorityQueue[pqi].push(fromNode);
	while (priorityQueue[pqi].size() > 0) {
		currentNode = priorityQueue[pqi].top();
		priorityQueue[pqi].pop();

		for (int i = 0; i < currentNode->connected.size(); i++) {
			tempNodeCost = currentNode->cost + currentNode->costs[i];
			if (!currentNode->connected[i]->visited) {
				currentNode->connected[i]->cost = tempNodeCost;
				currentNode->connected[i]->cameFrom = currentNode;
				currentNode->connected[i]->visited = true;
				currentNode->connected[i]->distanceFromGoal = fabs(currentNode->connected[i]->pos.distance(toNode->pos));
				
				priorityQueue[pqi].push(currentNode->connected[i]);
			} else if (tempNodeCost < currentNode->connected[i]->cost) {
				while (priorityQueue[pqi].size() > 0 && priorityQueue[pqi].top() != currentNode->connected[i]) {
					priorityQueue[1 - pqi].push(priorityQueue[pqi].top());
					priorityQueue[pqi].pop();
				}
				if(priorityQueue[pqi].size() > 0)
					priorityQueue[pqi].pop();

				if (priorityQueue[pqi].size()<priorityQueue[1 - pqi].size()) pqi = 1 - pqi;

				while (!priorityQueue[1-pqi].empty()) {
					priorityQueue[pqi].push(priorityQueue[1 - pqi].top());
					priorityQueue[1 - pqi].pop();
				}

				currentNode->connected[i]->cost = tempNodeCost;
				currentNode->connected[i]->cameFrom = currentNode;
				priorityQueue[pqi].push(currentNode->connected[i]);
			}
		}

		if (priorityQueue[1 - pqi].size() > 0 && priorityQueue[pqi].size() > 0) {
			if (compareNodes(priorityQueue[1 - pqi].top(), priorityQueue[pqi].top())) {
				pqi = 1 - pqi;
			}
		} else if (priorityQueue[1 - pqi].size() > 0 && priorityQueue[pqi].size() == 0) {
			pqi = 1 - pqi;
		}

		/*currentNode = priorityQueue[pqi].top();
		priorityQueue[pqi].pop();

		for (int i = 0; i < currentNode->connected.size(); i++) {
			tempNodeCost = currentNode->cost + currentNode->costs[i];
			if (!currentNode->connected[i]->visited) {
				currentNode->connected[i]->cost = tempNodeCost;
				currentNode->connected[i]->cameFrom = currentNode;
				currentNode->connected[i]->visited = true;
				currentNode->connected[i]->distanceFromGoal = currentNode->connected[i]->mapPos.distance(toNode->mapPos);

				priorityQueue[pqi].push(currentNode->connected[i]);
				topNode = priorityQueue[pqi].top();
				String();
			} else if (tempNodeCost < currentNode->connected[i]->cost) {
				while (!priorityQueue[pqi].empty() && priorityQueue[pqi].top() != currentNode->connected[i]) {
					priorityQueue[1 - pqi].push(priorityQueue[pqi].top());
					priorityQueue[pqi].pop();
				}
				if(!priorityQueue[pqi].empty())
					priorityQueue[pqi].pop();

				currentNode->connected[i]->cost = tempNodeCost;
				currentNode->connected[i]->cameFrom = currentNode;
				priorityQueue[1 - pqi].push(currentNode->connected[i]);
			}
		}
		if (!priorityQueue[pqi].empty() && !priorityQueue[1 - pqi].empty()) {
			if (compareNodes(priorityQueue[1 - pqi].top(), priorityQueue[pqi].top())) {
				pqi = 1 - pqi;
			}
		} else if (priorityQueue[1 - pqi].size() > 0 && priorityQueue[pqi].size() == 0) {
			pqi = 1 - pqi;
		}*/

		if (currentNode == toNode)
			break;
	}
	
	if (toNode->cameFrom) {
		currentNode = toNode;
		while (currentNode != fromNode) {
			path.push(currentNode);
			currentNode = currentNode->cameFrom;
		}
		path.push(fromNode);
	}

	return path;
}

Node* NodeNet::addNode(Vector2 pos, Vector2 mapPos) {
	Node* newNode = new Node(pos, mapPos);
	nodes.push_back(newNode);
	return newNode;
}

void NodeNet::addEdge(Node* node1, Node* node2, Number cost) {
	int idx1 = node1->getConnectionIndex(node2);
	if (idx1 >= 0) {
		Number newCost = (MAX(cost, node1->costs[idx1]) - MIN(cost, node1->costs[idx1])) / 2 + MIN(cost, node1->costs[idx1]);
		node1->costs[idx1] = newCost;
		node2->costs[node2->getConnectionIndex(node1)] = newCost;
	} else {
		node1->connected.push_back(node2);
		node1->costs.push_back(cost);
		node2->connected.push_back(node1);
		node2->costs.push_back(cost);
	}
}
