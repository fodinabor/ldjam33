#include "Intro.h"

Intro::Intro(Core *core) {
	this->core = core;

	introScene = new Scene(core, Scene::SCENE_2D);
	introScene->useClearColor = true;
	//introScene->clearColor = 

	ResourcePool *globalPool = core->getResourceManager()->getGlobalPool();

	texts.push_back(new SceneLabel(globalPool->getMaterial("Unlit"), "LUDUM DARE 33", 48, globalPool->getFont("Borea")));
	texts.push_back(new SceneLabel(globalPool->getMaterial("Unlit"), "THEME: YOU ARE THE MONSTER", 48, globalPool->getFont("Borea")));
	texts.push_back(new SceneLabel(globalPool->getMaterial("Unlit"), "MONSTER OR COSTUME?", 48, globalPool->getFont("Borea")));
	texts.push_back(new SceneLabel(globalPool->getMaterial("Unlit"), "A GAME BY JOACHIM MEYER", 48, globalPool->getFont("Borea")));
	texts.push_back(new SceneLabel(globalPool->getMaterial("Unlit"), "MADE POSSIBLE BY: POLYCODE AND TILED", 48, globalPool->getFont("Borea")));

	introScene->getActiveCamera()->setOrthoSize(core->getXRes(), core->getYRes());

	for (int i = 0; i < texts.size(); i++) {
		introScene->addChild(texts[i]);
		texts[i]->setColor(1, 1, 1, 0);
	}

	//timeToShow = 5.0;
	//fadeTime = 2.0;
	timeToShow = 2.0;
	fadeTime = 1.0;
	timeShowing = 0;
	timeFading = 0;
	idx = 0;
	showing = false;
	coming = true;
}

Intro::~Intro() { }

bool Intro::Update() {
	Number e = core->getElapsed();
	if (timeFading < fadeTime && !showing && coming) {
		timeFading += e;
		brightness += (1 / fadeTime) * e;
		texts[idx]->setColor(1.0, 1.0, 1.0, brightness);
		return true;
	} else if(timeFading < fadeTime && !showing && !coming) {
		timeFading += e;
		brightness -= (1 / fadeTime) * e;
		texts[idx]->setColor(1.0, 1.0, 1.0, brightness);
		return true;
	} else if (!showing && !coming) {
		idx++;
		if (idx == texts.size()) {
			introScene->enabled = false;
			return false;
		}
		coming = true;
		timeFading = 0;
		return true;
	}
	if (timeFading >= fadeTime && !showing)
		showing = true;
	if(showing)
		timeShowing += e;
	if (showing && timeShowing > timeToShow) {
		timeShowing = 0;
		showing = false;
		timeFading = 0;
		coming = false;
	}

	RenderFrame *frame = new RenderFrame(core->getViewport());
	introScene->Render(frame, nullptr, nullptr, nullptr, false);
	core->getRenderer()->submitRenderFrame(frame);

	return true;
}