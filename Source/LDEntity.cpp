#include "LDEntity.h"
#include "LDJAM33.h"
#include "LiveManager.h"

LDEntity::LDEntity(Core* core, SceneSprite* sprite, int type) {
	this->core = core;
	nodes = ldJam()->getMap()->getNodeNet();
	
	this->type = type;
	currentSprite = sprite;
	currentSprite->setSpriteStateByName("default", 0, false);
	currentSprite->backfaceCulled = false;
	addChild(currentSprite);

	if (type == TYPE_CIVILIAN){
		moveSpeed = 4;
	} else {
		moveSpeed = 2;
	}
	timeInState = 0;
	reactionSpeed = 2;
	alertState = ALERTSTATE_UNALERTED;
	visionAngle = PI;
	viewDist = 3;
	lastViewDir = Vector2(0, 0);
	lastKnownPos = Vector2(0, 0);
	state = STATE_STAYING;
	lastState = state;
	tileSize = 0;
	timeSinceHit = 0;
	timePossibleHit = 0;
}

LDEntity::~LDEntity() { }

void LDEntity::fixedUpdate() {
	timeInState += core->getElapsed();
	timeSinceHit += core->getElapsed();
	canMove();
	if (watchingFor)
		checkForWatching();
	else
		setWatchingFor(ldJam()->getMonster());
	setState();

	if (alertState == ALERTSTATE_HUNTING) {
		if (watchingFor) {
			findPath(watchingFor->getPosition2D());
			if (currentPath.size() > 0)
				currentPath.pop();
		}
	} else if (alertState == ALERTSTATE_UNALERTED) {
		if (currentPath.size() == 0) {
			currentPath.push(nodes->findNearestNode(getPosition2D())->getRandomConnectedNode());
			workingThroughPath = true;
		}
	}

	if (workingThroughPath) {
		if (currentPath.size() > 0) {
			goToPos(currentPath.top()->pos);
			if (getPosition2D().distance(currentPath.top()->pos) < tileSize / 2) {
				currentPath.pop();
				if (currentPath.size() == 0) {
					workingThroughPath = false;
				}
			}
		}
	}
	//currentSprite->Update();
}

void LDEntity::findPath(Vector2 to) {
	Vector2 start = getPosition2D();
	if (!canMoveDown)
		start.y += getHeight() / 2;
	if (!canMoveUp)
		start.y -= getHeight() / 2;
	if (!canMoveLeft)
		start.x += getWidth() / 2;
	if (!canMoveRight)
		start.x -= getWidth() / 2;
	currentPath = nodes->findPath(nodes->findNearestNode(start), nodes->findNearestNode(to));
	workingThroughPath = true;
}

void LDEntity::setWatchingFor(Entity * ent) {
	watchingFor = ent;
	watchingFor->addEventListener(this, 0);
}

void LDEntity::checkForWatching() {
	Vector2 distance = watchingFor->getPosition2D() - getPosition2D();
	distance.Normalize();
	Number angle = atan2(distance.y, distance.x);
	Number viewAngle = atan2(lastViewDir.y, lastViewDir.x);
	angle = viewAngle - angle;

	distance = getPosition2D();

	if (distance.distance(watchingFor->getPosition2D()) < viewDist*tileSize && fabs(angle) < visionAngle/2.0) {
		lastKnownPos = watchingFor->getPosition2D();
		
		if (alertState == ALERTSTATE_ALERTED) {
			if (timeInState > reactionSpeed / 2) {
				if (type == TYPE_WARRIOR) {
					alertState = ALERTSTATE_HUNTING;
				} else {
					alertState = ALERTSTATE_RUNNING;
				}
				timeInState = 0;
			}
		}

		if (alertState == ALERTSTATE_UNALERTED) {
			alertState = ALERTSTATE_ALERTED;
			timeInState = 0;
		}

		if (alertState == ALERTSTATE_SEEKING) {
			alertState = ALERTSTATE_HUNTING;
			timeInState = 0;
		}
		if (timeInState == 0) {
			dispatchEvent(new AlertEvent(alertState, lastKnownPos), ALERTSTATE_CHANGED_EVENT);
		}

	} else {
		if (alertState == ALERTSTATE_SEEKING) {
			if (timeInState > reactionSpeed * 3 && getPosition2D().distance(lastKnownPos) < tileSize/2) {
				alertState = ALERTSTATE_ALERTED;
				timeInState = 0;
			}
		}

		if (alertState == ALERTSTATE_HUNTING) {
			alertState = ALERTSTATE_SEEKING;
			findPath(lastKnownPos);
			timeInState = 0;
		}

		if (alertState == ALERTSTATE_RUNNING) {
			if (timeInState > reactionSpeed * 3) {
				alertState = ALERTSTATE_ALERTED;
				timeInState = 0;
			}
		}

		if (alertState == ALERTSTATE_ALERTED) {
			if (timeInState > reactionSpeed * 2) {
				alertState = ALERTSTATE_UNALERTED;
				timeInState = 0;
			}
		}
		if (timeInState == 0) {
			
			dispatchEvent(new AlertEvent(alertState, lastKnownPos), ALERTSTATE_CHANGED_EVENT);
		}
	}
}

void LDEntity::setState() {
	lastState = state;
	switch (alertState) {
	case ALERTSTATE_HUNTING:
		if (watchingFor && getPosition2D().distance(watchingFor->getPosition2D()) < tileSize) {
			state = STATE_HITTING;
			timePossibleHit += core->getElapsed();
			if (timeSinceHit > 2 && timePossibleHit > 1) {
				currentSprite->setSpriteStateByName("hit", 0, true);
				dispatchEvent(new LiveEvent(LiveEvent::WHO_MONSTER), LiveEvent::EVENT_DIE);
				timeSinceHit = 0;
			}
		} else {
			state = STATE_RUNNING;
			if(state != lastState)
			currentSprite->setSpriteStateByName("run", 0, false);
			timePossibleHit = 0;
		}
		break;
	case ALERTSTATE_SEEKING:
	case ALERTSTATE_UNALERTED:
		state = STATE_MOVING;
		if (state != lastState)
		currentSprite->setSpriteStateByName("move", 0, false);
		break;
	case ALERTSTATE_ALERTED:
		state = STATE_STAYING;
		if (state != lastState)
		currentSprite->setSpriteStateByName("default", 0, false);
		break;
	case ALERTSTATE_RUNNING:
		if (state != STATE_RUNNING || !workingThroughPath) {
			state = STATE_RUNNING;
			//Roll(180);
			findPath(Vector2(rand() % ldJam()->getMap()->getMapSize() * tileSize, rand() % ldJam()->getMap()->getMapSize() * tileSize));
			currentSprite->setSpriteStateByName("run", 0, false);
			//dispatchEvent(new AlertEvent(alertState, lastKnownPos), ALERTSTATE_RUNNING_EVENT);
		}
		break;
	}
}

void LDEntity::handleEvent(Event * e) {
	if (enabled) {
		if (e->getDispatcher() == watchingFor) {
			Vector2 hitPos = ((HitEvent*) e)->pos;
			Vector2 view = ((HitEvent*) e)->view;
			Vector2 distance = hitPos + Vector2(watchingFor->getHeight() / 2, watchingFor->getHeight() / 2) - getPosition2D();
			distance.Normalize();
			Number angle = atan2(distance.y, distance.x);
			Number viewAngle = atan2(view.y, view.x);
			angle = viewAngle - angle;
			if (getPosition2D().distance(hitPos) < tileSize && fabs(angle) < visionAngle / 2.0) {
				dispatchEvent(new LiveEvent(type + 1), LiveEvent::EVENT_DIE);
			}
		}
	}
}

void LDEntity::goToPos(const Vector2 pos) {
	Vector2 target = pos - getPosition2D();
	Number stepSize;
	
	if (!canMoveDown && target.y < 0)
		target.y = 0;
	if (!canMoveUp && target.y > 0)
		target.y = 0;
	if (!canMoveLeft && target.x < 0)
		target.x = 0;
	if (!canMoveRight && target.x > 0)
		target.x = 0;
	target.Normalize();
	lastViewDir = target;

	if (state == STATE_RUNNING) {
		stepSize = moveSpeed * 2;
		if (lastState != STATE_RUNNING) {
			lastState = state;
			currentSprite->setSpriteStateByName("run", 0, false);
		}
	} else {
		stepSize = (moveSpeed);
		if (state != STATE_MOVING) {
			lastState = state;
			state = STATE_MOVING;
			currentSprite->setSpriteStateByName("move", 0, false);
		}
	}

	target.x *= stepSize;
	target.y *= stepSize;

	if (ldJam()->getMap()->canPassTile(getPosition2D() + target)) {
		stepSize = ldJam()->getMap()->getTileCost(getPosition2D() + target);
	}

	if (!canMoveDown && target.y < 0)
		target.y = -target.y;
	if (!canMoveUp && target.y > 0)
		target.y = -target.y;
	if (!canMoveLeft && target.x < 0)
		target.x = -target.x;
	if (!canMoveRight && target.x > 0)
		target.x = -target.x;

	if (target.x > 0) {
		currentSprite->setScaleX(1);
	} else if (target.x < 0) {
		currentSprite->setScaleX(-1);
	}

	this->Translate(target.x / stepSize, target.y / stepSize);
}

void LDEntity::setTileSize(int s) {
	tileSize = s;
}

void LDEntity::alertByRunning(Vector2 lastPos) { 
	if (alertState != ALERTSTATE_HUNTING && type == TYPE_WARRIOR) {
		lastKnownPos = lastPos;
		alertState = ALERTSTATE_SEEKING;
		findPath(lastKnownPos);
	}
}

void LDEntity::canMove() {
	Vector2 position2d = getPosition2D();
	canMoveRight = false;
	canMoveLeft = false;
	canMoveUp = false;
	canMoveDown = false;

	if (ldJam()->getMap()->canPassTile(position2d + Vector2(currentSprite->getWidth() / 2, /*-currentSprite->getHeight() / 2 + 5*/0)) &&
		ldJam()->getMap()->canPassTile(position2d + Vector2(currentSprite->getWidth() / 2, currentSprite->getHeight() / 2 - 5))) {
		canMoveRight = true;
	}
	if (ldJam()->getMap()->canPassTile(position2d - Vector2(currentSprite->getWidth() / 2,/* currentSprite->getHeight() / 2 - 5*/0)) &&
		ldJam()->getMap()->canPassTile(position2d - Vector2(currentSprite->getWidth() / 2, -currentSprite->getHeight() / 2 + 5))) {
		canMoveLeft = true;
	}
	if (ldJam()->getMap()->canPassTile(position2d + Vector2(currentSprite->getWidth() / 2 - 5, currentSprite->getHeight() / 2 + 5)) &&
		ldJam()->getMap()->canPassTile(position2d + Vector2(-currentSprite->getWidth() / 2 + 5, currentSprite->getHeight() / 2 + 5))) {
		canMoveUp = true;
	}
	if (ldJam()->getMap()->canPassTile(position2d + Vector2(currentSprite->getWidth() / 2 - 5, -5)) &&
		ldJam()->getMap()->canPassTile(position2d + Vector2(-currentSprite->getWidth() / 2 + 5, -5))) {
		canMoveDown = true;
	}
}