#include "LiveManager.h"

LiveManager::LiveManager(Core *core, Scene *s) : UIElement(core) {
	main = s;

	enabled = false;

	kE = new SpriteSet(core, "NPCs.sprites", core->getResourceManager()->getGlobalPool());

	m = new SpriteSet(core, "Monster.sprites", core->getResourceManager()->getGlobalPool());

	livesKlaus = 0;
	livesEmil = 0;
	livesMonster = 0;
	//setLivesEmil(5);
	//setLivesKlaus(5);
	//setLivesMonster(3);
}


LiveManager::~LiveManager() { }

void LiveManager::killKlaus() {
	livesKlaus--;
	if (livesKlaus >= 0) {
		livesK[livesKlaus]->setSpriteStateByName("dead",0,false);
		if (livesKlaus == 0)
			dispatchEvent(new LiveEvent(LiveEvent::WHO_KLAUS), Event::COMPLETE_EVENT);
	} else {
		dispatchEvent(new LiveEvent(LiveEvent::WHO_KLAUS), Event::COMPLETE_EVENT);
	}
}

void LiveManager::killEmil() {
	livesEmil--;
	if (livesEmil >= 0) {
		livesE[livesEmil]->setSpriteStateByName("dead", 0, false);
		if (livesEmil == 0)
			dispatchEvent(new LiveEvent(LiveEvent::WHO_EMIL), Event::COMPLETE_EVENT);
	} else {
		dispatchEvent(new LiveEvent(LiveEvent::WHO_EMIL), Event::COMPLETE_EVENT);
	}
}

void LiveManager::killMonster() {
	livesMonster--;
	if (livesMonster >= 0) {
		livesM[livesMonster]->setSpriteStateByName("dead", 0, false);
		if(livesMonster == 0)
			dispatchEvent(new LiveEvent(LiveEvent::WHO_MONSTER), Event::COMPLETE_EVENT);
	} else {
		dispatchEvent(new LiveEvent(LiveEvent::WHO_MONSTER), Event::COMPLETE_EVENT);
	}
}

void LiveManager::setLivesKlaus(int l) {
	for (int i = 0; i < livesK.size(); i++) {
		removeChild(livesK[i]);
	}
	livesK.clear();
	int s = livesK.size();
	for (int i = s; i < l; i++) {
		SceneSprite* head = new SceneSprite(kE, core->getResourceManager()->getGlobalPool()->getMaterial("Unlit"));
		head->setSpriteByName("KlausLives");
		head->setSpriteStateByName("default", 0, false);
		head->setScale(0.2, 0.2);
		head->setPositionX(i*(head->getWidth()*0.3) + 20);
		head->setPositionY((head->getHeight()*0.3)*2);
		addChild(head);
		livesK.push_back(head);
	}

	for (int i = 0; i < livesK.size(); i++) {
		livesK[i]->setSpriteStateByName("default", 0, false);
	}
	livesKlaus = l;
}

void LiveManager::setLivesEmil(int l) {
	for (int i = 0; i < livesE.size(); i++) {
		removeChild(livesE[i]);
	}
	livesE.clear();
	int s = livesE.size();
	for (int i = s; i < l; i++) {
		SceneSprite* head = new SceneSprite(kE, core->getResourceManager()->getGlobalPool()->getMaterial("Unlit"));
		head->setSpriteByName("EmilLives");
		head->setSpriteStateByName("default", 0, false);
		head->setScale(0.2, 0.2);
		head->setPositionX(i*(head->getWidth()*0.3) + 20);
		head->setPositionY((head->getHeight()*0.3)*3);
		addChild(head);
		livesE.push_back(head);
	}
	for (int i = 0; i < livesE.size(); i++) {
		livesE[i]->setSpriteStateByName("default", 0, false);
	}
	livesEmil = l;
}

void LiveManager::setLivesMonster(int l) {
	for (int i = 0; i < livesM.size(); i++) {
		removeChild(livesM[i]);
	}
	livesM.clear();
	int s = livesM.size();
	for (int i = s; i < l; i++) {
		SceneSprite* head = new SceneSprite(m, core->getResourceManager()->getGlobalPool()->getMaterial("Unlit"));
		head->setSpriteByName("MonsterLive");
		head->setSpriteStateByName("default", 0, false);
		head->setScale(0.3, 0.3);
		head->setPositionX(i*(head->getWidth()*0.4) + 20);
		head->setPositionY((head->getHeight()*0.4));
		addChild(head);
		livesM.push_back(head);
	}
	
	for (int i = 0; i < livesM.size(); i++) {
		livesM[i]->setSpriteStateByName("default", 0, false);
	}
	livesMonster = l;
}

int LiveManager::getLivesKlaus() {
	return livesKlaus;
}

int LiveManager::getLivesEmil() {
	return livesEmil;
}

int LiveManager::getLivesMonster() {
	return livesMonster;
}

void LiveManager::handleEvent(Event * e) {
	if (e->getEventType() == "LiveEvent") {
		LiveEvent* le = (LiveEvent*) e;

		if (le->who == LiveEvent::WHO_MONSTER) {
			killMonster();
		} else {
			if (le->who == LiveEvent::WHO_EMIL) {
				killEmil();
			} else if (le->who == LiveEvent::WHO_KLAUS) {
				killKlaus();
			}
			main->removeEntity((Entity*) e->getDispatcher());
			((Entity*) e->getDispatcher())->enabled = false;
		}
	}
}
