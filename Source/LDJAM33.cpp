#include "LDJAM33.h"
#include "Monster.h"
#include "LDEntity.h"
#include "Map.h"
#include <time.h>

LDJAM33 *LDJAM33::overrideInstance = NULL;

LDJAM33 *ldJam() {
	return LDJAM33::getInstance();
}

LDJAM33 *LDJAM33::getInstance() {
	if (overrideInstance) {
		return overrideInstance;
	}
}

LDJAM33::LDJAM33(PolycodeView *view) {
	
	state = 0;
	mapReady = 0;
	dotCounter = 0;
	dotDelay = 0;
	showTime = 0;
	wonLost = false;

	Logger::getInstance()->setLogToFile(true);

	core = new POLYCODE_CORE(view, 1000,750,false, false, 0, 0,60);	  
	//CoreServices::getInstance()->getMaterialManager()->setTextureFilteringMode(Texture::FILTERING_NEAREST);

	//srand(time(NULL));
	//core->Render();
	ResourcePool *globalPool = core->getResourceManager()->getGlobalPool();
	//core->addFileSource("folder", "Resources");
	globalPool->loadResourcesFromFolder("", false);
	Logger::log("Resources\n");
	core->addFileSource("archive", "default.pak");
	globalPool->loadResourcesFromFolder("default", false);
	
	globalPool->loadFont("Borea", "Borea.ttf");

	overrideInstance = this;

	intro = new Intro(core);
	introMusic = new Sound(core,"IntroMusik.OGG");
	introMusic->Play(false);

	fightMusic = new Sound(core, "FightBG.OGG");
	normalMusic = new Sound(core, "NormalBG.OGG");

	scene = new Scene(core, Scene::SCENE_2D);
	scene->getActiveCamera()->setOrthoSize(core->getXRes(), core->getYRes());
	scene->doVisibilityChecking(false);
	scene->enabled = false;
	scene->useClearColor = true;

	ui = new Scene(core, Scene::SCENE_2D_TOPLEFT);
	ui->enabled = false;
	ui->rootEntity.processInputEvents = true;

	String controlsText = "Controls: Arrow keys or mouseclick to move, shift to sprint, space to attack";

	controls = new UILabel(core, globalPool, controlsText, 16, "mono");
	controls->setPosition(5, 8);
	controls->setColor(1.0, 1.0, 1.0, 1.0);
	ui->addChild(controls);

	menu = new UIElement(core);
	ui->addChild(menu);
	menu->setPosition(200, 100);

	startGame = new UILabel(core, globalPool, "Start", 32, "Borea");
	menu->addChild(startGame);
	startGame->setPosition(100, 80);
	startGame->setColor(0, 1, 0, 1);
	startGame->processInputEvents = true;
	startGame->addEventListener(this, InputEvent::EVENT_MOUSEOVER);
	startGame->addEventListener(this, InputEvent::EVENT_MOUSEDOWN);

	closeGame = new UILabel(core, globalPool, "Beenden", 32, "Borea");
	menu->addChild(closeGame);
	closeGame->setPosition(200, 160);
	closeGame->setColor(1, 0, 0, 0.5);
	closeGame->processInputEvents = true;
	closeGame->addEventListener(this, InputEvent::EVENT_MOUSEOVER);
	closeGame->addEventListener(this, InputEvent::EVENT_MOUSEDOWN);

	liveManager = new LiveManager(core, scene);
	liveManager->addEventListener(this, Event::COMPLETE_EVENT);
	ui->addChild(liveManager);

	monster = new Monster(core);
	//monster->enabled = false;
	
	map = new Map(core, "map.tmx", scene);
	map->addEventListener(this, Event::COMPLETE_EVENT);

	loading = new SceneLabel(globalPool->getMaterial("Unlit"), "Loading", 48, globalPool->getFont("Borea"));
	ui->addChild(loading);
	loading->setPosition(core->getXRes() / 2 /*- won->getWidth() / 2*/, core->getYRes() / 2 );

	won = new SceneLabel(globalPool->getMaterial("Unlit"), "YOU ARE A MONSTER! YOU KILLED ALL OF THEM!!! - Well done ;-)", 30, globalPool->getFont("Borea"));
	ui->addChild(won);
	won->enabled = false;
	won->setPosition(core->getXRes() / 2 /*- won->getWidth() / 2*/, core->getYRes() / 2 );
	lost = new SceneLabel(globalPool->getMaterial("Unlit"), "Well... maybe youre just a human in a costume ... well have been - you're dead!", 25, globalPool->getFont("Borea"));
	ui->addChild(lost);
	lost->setPosition(core->getXRes() / 2 /*- lost->getWidth() / 2*/, core->getYRes() / 2);
	lost->enabled = false;

	core->getInput()->addEventListener(this, InputEvent::EVENT_MOUSEDOWN);

	state = SHOW_INTRO;

	dotDelay = 0;
	dotCounter = 0;
	showTime = 0;
}
LDJAM33::~LDJAM33() {
    
}

bool LDJAM33::Update() {
	bool res = core->Update();
	RenderFrame *frame = new RenderFrame(core->getViewport());

	showTime += core->getElapsed();
	alertTime += core->getElapsed();
	if(state == SHOW_INTRO) {
		if(!intro->Update() || mapReady) {
			state = SHOW_MENU;
			showTime = 0;
			introMusic->Stop();
			normalMusic->Play(true);
			ui->enabled = true;
			ui->useClearColor = true;
		}
	} else if(state == SHOW_MENU) {
		ui->Render(frame, NULL, NULL, NULL, false);
	} else if(!mapReady) {
		if(dotCounter < 5) {
			if(dotDelay > 1.0) {
				loading->setText(loading->getText() + ".");
				dotCounter++;
				dotDelay = 0;
			}
			dotDelay += core->getElapsed();
		} else {
			loading->setText("Loading");
			dotCounter = 0;
		}
		ui->Render(frame, NULL, NULL, NULL, false);
	} else if(state == SHOW_GAME) {
		if(alertTime > 10.0 && (normalMusic->getVolume() < 0.95 || !normalMusic->isPlaying())) {
			fightMusic->setVolume(fightMusic->getVolume() - core->getElapsed() * 0.5);
			normalMusic->setVolume(1.0 - fightMusic->getVolume());
		}
		if(!normalMusic->isPlaying() && (!fightMusic->isPlaying() || alertTime > 10.0))
			normalMusic->Play(true);
		if(core->getInput()->getKeyState(KEY_F3)) {
			if(map->getNodeNet()->getShowConnections()) {
				scene->removeEntity(map->getNodeNet()->showConnections(core, false));
			} else {
				scene->addChild(map->getNodeNet()->showConnections(core, true));
			}
		}
		scene->Update(core->getElapsed());
		scene->fixedUpdate();
		scene->Render(frame, NULL, NULL, NULL, false);
		ui->Render(frame, NULL, NULL, NULL, false);
	} else if(state == SHOW_LOOSE) {
		if(showTime > 10) {
			state = SHOW_MENU;
			showTime = 0;
			lost->enabled = false;
			menu->enabled = true;
		}
		ui->Render(frame, NULL, NULL, NULL, false);
	} else if(state == SHOW_WON) {
		if(showTime > 10) {
			state = SHOW_MENU;
			showTime = 0;
			won->enabled = false;
			menu->enabled = true;
		}
		ui->Render(frame, NULL, NULL, NULL, false);
	}

	scene->getActiveCamera()->setPositionX(monster->getPosition().x);
	scene->getActiveCamera()->setPositionY(monster->getPosition().y);

	//monster->Translate(0.1, 0.1);

	////scene->Render(frame, NULL, NULL, NULL, false);
	if(state != SHOW_INTRO)
		core->getRenderer()->submitRenderFrame(frame);

	return res;
}

void LDJAM33::handleEvent(Event * e) {
	if (e->getDispatcher() == startGame && state == SHOW_MENU) {
		InputEvent* ie = (InputEvent*) e;
		if (ie->getEventCode() == InputEvent::EVENT_MOUSEOVER) {
			startGame->setColor(0, 1, 0, 1);
			closeGame->setColor(1, 0, 0, 0.5);
		}
		if (ie->getEventCode() == InputEvent::EVENT_MOUSEDOWN) {
			state = SHOW_GAME;
			if (wonLost) {
				map->threadRunning = true;
				core->createThread(map);
			}
			ui->useClearColor = false;
			scene->enabled = true;
			menu->enabled = false;
			controls->enabled = false;
			liveManager->enabled = true;
			showTime = 0;
		}
	} else if (e->getDispatcher() == closeGame && state == SHOW_MENU) {
		InputEvent* ie = (InputEvent*) e;
		if (ie->getEventCode() == InputEvent::EVENT_MOUSEOVER) {
			startGame->setColor(0, 1, 0, 0.5);
			closeGame->setColor(1, 0, 0, 1);
		}
		if (ie->getEventCode() == InputEvent::EVENT_MOUSEDOWN) {
			core->Shutdown();
		}
	}
	if (e->getDispatcher() == core->getInput()) {
		InputEvent* ie = (InputEvent*) e;
		if (ie->getEventCode() == InputEvent::EVENT_MOUSEDOWN) {
		  if(mapReady){
			Vector3 temp = scene->projectRayFromCameraAndViewportCoordinate(scene->getActiveCamera(), ie->getMousePosition()).origin;
			monster->findPath(Vector2(temp.x, temp.y));
		  }
		}
	} else if (e->getDispatcher() == map) {
		if(!mapReady)
			scene->addChild(monster);
		monster->setPosition(0, 0);
		mapReady = true;
		//scene->enabled = true;
		loading->enabled = false;
		loading->visible = false;
		monster->setTileSize(map->getTileSize());
		monster->setNodes(map->getNodeNet());
	} else if (e->getEventType() == "AlertEvent") {
		AlertEvent* ae = (AlertEvent*) e;
		if (ae->alertState > 0) {
			if (normalMusic->isPlaying())
				normalMusic->Stop();
			if (!fightMusic->isPlaying())
				fightMusic->Play(true);
			alertTime = 0;
			fightMusic->setVolume(1.0);
		}
		if (ae->alertState == LDEntity::ALERTSTATE_RUNNING) {
			for (int i = 0; i < lds.size(); i++) {
				lds[i]->alertByRunning(ae->lastPos);
			}
		}
	} else if (e->getDispatcher() == liveManager) {
		LiveEvent* le = (LiveEvent*)e;
		if ((le->who == LiveEvent::WHO_EMIL && liveManager->getLivesKlaus() == 0) || (le->who == LiveEvent::WHO_KLAUS && liveManager->getLivesEmil() == 0)) {
			state = SHOW_WON;
			showTime = 0;
			ui->useClearColor = true;
			won->enabled = true;
			scene->enabled = false;
			wonLost = true;
		} else if(le->who == LiveEvent::WHO_MONSTER) {
			state = SHOW_LOOSE;
			showTime = 0;
			ui->useClearColor = true;
			lost->enabled = true;
			scene->enabled = false;
			wonLost = true;
		}
	}
}

Map * LDJAM33::getMap() {
	return map;
}

Monster * LDJAM33::getMonster() {
	return monster;
}

LiveManager * LDJAM33::getLiveManager() {
	return liveManager;
}

