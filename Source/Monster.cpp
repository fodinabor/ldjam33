#include "Monster.h"

Monster::Monster(Core *core){
	this->core = core;

	tileSize = 0;
	lastState = 0;

	nodes = NULL;
	input = core->getInput();

	spriteSet = new SpriteSet(core, "Monster.sprites", core->getResourceManager()->getGlobalPool());
	currentSprite = new SceneSprite(spriteSet, core->getResourceManager()->getGlobalPool()->getMaterial("Unlit"));
	currentSprite->setSpriteByName("Monster");
	currentSprite->setSpriteStateByName("default", 0, false);
	currentSprite->backfaceCulled = false;
	addChild(currentSprite);

	moveSpeed = 3;
	timeInState = 0;
	reactionTime = 2;
	state = STATE_STAYING;
}

Monster::~Monster() { }

void Monster::goToPos(Vector2 target) {
	target = target - getPosition2D();
	if (!canMoveDown && target.y < 0)
		target.y = 0;
	if (!canMoveUp && target.y > 0)
		target.y = 0;
	if (!canMoveLeft && target.x < 0)
		target.x = 0;
	if (!canMoveRight && target.x > 0)
		target.x = 0;
	target.Normalize();
	Number stepSize;

	if (state == STATE_RUNNING) {
		stepSize = moveSpeed * 2;
		if (lastState != STATE_RUNNING) {
			lastState = state;
			currentSprite->setSpriteStateByName("run", 0, false);
		}
	} else {
		stepSize = (moveSpeed);
		if (state != STATE_MOVING) {
			lastState = state;
			state = STATE_MOVING;
			currentSprite->setSpriteStateByName("move", 0, false);
		}
	}

	target.x *= stepSize;
	target.y *= stepSize;

	if (ldJam()->getMap()->canPassTile(getPosition2D() + target)) {
		stepSize = ldJam()->getMap()->getTileCost(getPosition2D() + target);
	}

	if (!canMoveDown && target.y < 0)
		target.y = -target.y;
	if (!canMoveUp && target.y > 0)
		target.y = -target.y;
	if (!canMoveLeft && target.x < 0)
		target.x = -target.x;
	if (!canMoveRight && target.x > 0)
		target.x = -target.x;

	if (target.x > 0) {
		currentSprite->setScaleX(1);
	} else if (target.x < 0) {
		currentSprite->setScaleX(-1);
	}
	lastDir = target;
	this->Translate(target.x / stepSize, target.y / stepSize);
}

void Monster::fixedUpdate() {
	state = STATE_STAYING;
	timeInState += core->getElapsed();

	Vector2 position2d = this->getPosition2D();
	Vector2 target(0, 0);

	canMove();

	if ((input->getKeyState(::KEY_LSHIFT) || input->getKeyState(::KEY_RSHIFT)) && 
		(workingThroughPath || input->getKeyState(::KEY_UP) || input->getKeyState(::KEY_DOWN) || 
			input->getKeyState(::KEY_RIGHT) || input->getKeyState(::KEY_LEFT))) {
		lastState = state;
		state = STATE_RUNNING;
	}
	if (input->getKeyState(::KEY_UP)) {
		workingThroughPath = false;
		goToPos(Vector2(position2d.x, position2d.y+tileSize));
	}
	if (input->getKeyState(::KEY_DOWN)) {
		workingThroughPath = false;
		goToPos(Vector2(position2d.x, position2d.y -tileSize));
	}
	if (input->getKeyState(::KEY_RIGHT)) {
		workingThroughPath = false;
		goToPos(Vector2(position2d.x+tileSize, position2d.y));
	}
	if (input->getKeyState(::KEY_LEFT)) {
		workingThroughPath = false;
		goToPos(Vector2(position2d.x - tileSize, position2d.y));
	}
	if (input->getKeyState(::KEY_SPACE)) {
		state = STATE_HITTING;
		if (lastState != state)
			currentSprite->setSpriteStateByName("hit", 0, true);
		dispatchEvent(new HitEvent(getPosition2D(), lastDir), 0);
	}
	
	if (workingThroughPath) {
		if (currentPath.size() > 0) {
			goToPos(currentPath.top()->pos);
			if (getPosition2D().distance(currentPath.top()->pos) < tileSize / 2) {
				currentPath.pop();
				if (currentPath.size() == 0) {
					workingThroughPath = false;
				}
			}
		}
	}

	if (state == STATE_STAYING && currentSprite->getCurrentSpriteState()->getName() != "hit") {
		currentSprite->setSpriteStateByName("default",0,false);
	}
	//currentSprite->Update();
}

//void Monster::Update() {
//	//Entity::doUpdates();
//}

void Monster::canMove() {
	Vector2 position2d = getPosition2D();
	canMoveRight = false;
	canMoveLeft = false;
	canMoveUp = false;
	canMoveDown = false;

	if (ldJam()->getMap()->canPassTile(position2d + Vector2(currentSprite->getWidth() / 2, /*-currentSprite->getHeight() / 2 + 5*/0)) &&
		ldJam()->getMap()->canPassTile(position2d + Vector2(currentSprite->getWidth() / 2, currentSprite->getHeight() / 2 - 5))) {
		canMoveRight = true;
	}
	if (ldJam()->getMap()->canPassTile(position2d - Vector2(currentSprite->getWidth() / 2,/* currentSprite->getHeight() / 2 - 5*/0)) &&
		ldJam()->getMap()->canPassTile(position2d - Vector2(currentSprite->getWidth() / 2, -currentSprite->getHeight() / 2+5))) {
		canMoveLeft = true;
	}
	if (ldJam()->getMap()->canPassTile(position2d + Vector2(currentSprite->getWidth() / 2 - 5, currentSprite->getHeight() / 2 + 5)) &&
		ldJam()->getMap()->canPassTile(position2d + Vector2(-currentSprite->getWidth() / 2 + 5, currentSprite->getHeight() / 2 + 5))) {
		canMoveUp = true;
	}
	if (ldJam()->getMap()->canPassTile(position2d + Vector2(currentSprite->getWidth() / 2 - 5, -5)) &&
		ldJam()->getMap()->canPassTile(position2d + Vector2(-currentSprite->getWidth() / 2 + 5 , -5))) {
		canMoveDown = true;
	}
}

void Monster::findPath(Vector2 to) {
	Vector2 start = getPosition2D();
	if (!canMoveDown)
		start.y += getHeight() / 2;
	if (!canMoveUp)
		start.y -= getHeight() / 2;
	if (!canMoveLeft)
		start.x += getWidth() / 2;
	if (!canMoveRight)
		start.x -= getWidth() / 2;
	currentPath = nodes->findPath(nodes->findNearestNode(start), nodes->findNearestNode(to));
	workingThroughPath = true;
}

void Monster::setTileSize(int s) {
	tileSize = s;
}

void Monster::setNodes(NodeNet * nod) {
	nodes = nod;
}

void Monster::handleEvent(Event* e) {

}