#include "Map.h"
#include "LDEntity.h"
#include "LDJAM33.h"
#include "LiveManager.h"

Map::Map(Core* core, String mapDataFile, Scene* scene) : Threaded() {
	this->core = core;

	nodes = new NodeNet();
	tileInfo = new TileInfoManager();
	this->ld = ldJam();
	
	this->scene = scene;
	this->mapDataFile = mapDataFile;
	
	ground = new SceneMesh();
	ground->getMesh()->addSubmesh(MeshGeometry(MeshGeometry::TRISTRIP_MESH));
	ground->billboardMode = true;
	//ground->billboardRoll = true;
	ground->setMaterial(core->getResourceManager()->getGlobalPool()->getMaterial("Unlit"));
	ground->getShaderPass(0).shaderBinding->setTextureForParam("diffuse", core->getResourceManager()->getGlobalPool()->loadTexture("MapTiles.png"));

	sprites = new SpriteSet(core, "NPCs.sprites", core->getResourceManager()->getGlobalPool());

	loadMapData = true;
	sizeX = 0;
	sizeY = 0;
	tileData = NULL;
	tileWidth = 0;
	tileHeight = 0;

	core->createThread(this);
}

Map::~Map() { }

NodeNet * Map::getNodeNet() const {
	return nodes;
}

int Map::getMapSize() const {
	return MIN(sizeX,sizeY);
}

Vector2 Map::worldToTileCoordinates(Vector2 world) {
	world.x = round((world.x - 0.5 * tileWidth + (sizeX*tileWidth / 2)) / tileWidth);
	world.y = round(-(world.y + 0.5 * tileWidth - (sizeY*tileHeight / 2)) / tileHeight);
	return world;
}

int Map::getTileSize() const {
	return tileWidth;
}

int Map::getTileCost(Vector2 tilePos) {
	tilePos = worldToTileCoordinates(tilePos);
	int x = tilePos.x;
	int y = tilePos.y;
	if (x >= 0 && y >= 0 && x < sizeX && y < sizeY) {
		return tileInfo->getTileInfo(tileData[x][y]).cost;
	}
	return -1;
}

bool Map::canPassTile(Vector2 tilePos) {
	tilePos = worldToTileCoordinates(tilePos);
	int x = tilePos.x;
	int y = tilePos.y;
	if (x >= 0 && y >= 0 && x < sizeX && y < sizeY) {
		return tileInfo->getTileInfo(tileData[x][y]).passable;
	}
	return false;
}

void Map::updateThread() {
	Object data = Object();
	data.loadFromXML(core, mapDataFile);
	
	if (loadMapData) {
		ObjectEntry* layer;
		ObjectEntry* layerData;


		layer = data.root["layer"];
		tileWidth = data.root["tilewidth"]->intVal;
		tileHeight = data.root["tileheight"]->intVal;

		if ((*layer)["name"]->stringVal.toLowerCase() == String("Tiles").toLowerCase()) {
			sizeX = (*layer)["width"]->intVal;
			sizeY = (*layer)["height"]->intVal;

			tileData = new int*[sizeX];

			layerData = (*layer)["data"];
			for (int i = 0; i < sizeX; i++) {
				tileData[i] = new int[sizeY];
				for (int j = 0; j < sizeY; j++) {
					if (i*j < layerData->length) {
						tileData[i][j] = (*(*layerData)[i + j*sizeY])["gid"]->intVal;

						Number cellSize = 0.1;

						Number texX = (((tileData[i][j] - 1) % 10)) * cellSize;
						Number texY = floor((float) tileData[i][j] / 10.0) * cellSize;

						ground->getMesh()->getSubmeshPointer(0)->addVertexWithUV(-(sizeX*tileWidth / 2) + i*tileWidth, sizeY*tileHeight / 2 - j*tileHeight, 0, texX, 1.0 - texY); // links oben
						ground->getMesh()->getSubmeshPointer(0)->addVertexWithUV(-(sizeX*tileWidth / 2) + i*tileWidth, sizeY*tileHeight / 2 - (j*tileHeight + tileHeight), 0, texX, 1.0 - (texY + cellSize)); // links unten
						ground->getMesh()->getSubmeshPointer(0)->addVertexWithUV(-(sizeX*tileWidth / 2) + i*tileWidth + tileWidth, sizeY*tileHeight / 2 - j*tileHeight, 0, texX + cellSize, 1.0 - texY); // rechts oben
						ground->getMesh()->getSubmeshPointer(0)->addVertexWithUV(-(sizeX*tileWidth / 2) + i*tileWidth + tileWidth, sizeY*tileHeight / 2 - (j*tileHeight + tileHeight), 0, texX + cellSize, 1.0 - (texY + cellSize)); // rechts unten

						if (tileInfo->getTileInfo(tileData[i][j]).passable) {
							nodes->addNode(Vector2(-(sizeX*tileWidth) / 2 + i*tileWidth + (tileWidth / 2), (sizeY*tileHeight) / 2 - j*tileHeight - (tileHeight / 2)), Vector2(i, j));
						}
					}
				}
			}
			ground->getMesh()->getSubmeshPointer(0)->calculateNormals();
			scene->addChild(ground);

			Node* currentNode;
			Node* next;
			TileInfo currentTile;
			int xS, yS;
			for (int x = 0; x < sizeX; x++) {
				for (int y = 0; y < sizeY; y++) {
					currentTile = tileInfo->getTileInfo(tileData[x][y]);
					if (currentTile.passable) {
						xS = -(sizeX*tileWidth) / 2 + x*tileWidth + (tileWidth / 2);
						yS = (sizeY*tileHeight) / 2 - y*tileHeight - (tileHeight / 2);
						currentNode = nodes->findNearestNode(Vector2(xS, yS));
						if (x - 1 >= 0 && tileInfo->getTileInfo(tileData[x - 1][y]).passable) {
							next = nodes->findNearestNode(Vector2(xS - tileWidth, yS));
							nodes->addEdge(currentNode, next, currentTile.cost);
						}
						if (y - 1 >= 0 && tileInfo->getTileInfo(tileData[x][y - 1]).passable) {
							next = nodes->findNearestNode(Vector2(xS, yS + tileHeight));
							nodes->addEdge(currentNode, next, currentTile.cost);
						}
						if (x + 1 < sizeX && tileInfo->getTileInfo(tileData[x + 1][y]).passable) {
							next = nodes->findNearestNode(Vector2(xS + tileWidth, yS));
							nodes->addEdge(currentNode, next, currentTile.cost);
						}
						if (y + 1 < sizeY&& tileInfo->getTileInfo(tileData[x][y + 1]).passable) {
							next = nodes->findNearestNode(Vector2(xS, yS - tileHeight));
							nodes->addEdge(currentNode, next, currentTile.cost);
						}
					}
				}
			}
		}
	}

	ObjectEntry* objGroup;
	LDEntity* currentEntity;
	SceneSprite* sprite;
	int type;
	ObjectEntry* objectEntry;
	int anzWar = 0;
	int anzCiv = 0;

	for (int i = 0; i < ld->lds.size(); i++) {
		scene->removeEntity(ld->lds[i]);
	}

	objGroup = data.root["objectgroup"];
	if ((*objGroup)["name"]->stringVal.toLowerCase() == String("Objects").toLowerCase()) {
		for (int i = 1; i < objGroup->length + 1; i++) {
			objectEntry = (*objGroup)[i];
			type = (*objectEntry)["type"]->intVal;
			sprite = new SceneSprite(sprites, core->getResourceManager()->getGlobalPool()->getMaterial("Unlit"));

			if (type == LDEntity::TYPE_WARRIOR) {
				sprite->setSpriteByName("Emil");
				anzWar++;
			} else {
				sprite->setSpriteByName("Klaus");
				anzCiv++;
			}

			currentEntity = new LDEntity(core, sprite, type);
			currentEntity->setWatchingFor(this->ld->getMonster());
			currentEntity->setPosition((*objectEntry)["x"]->intVal-sizeX*tileWidth/2, sizeY*tileHeight/2-(*objectEntry)["y"]->intVal, 0);
			currentEntity->setTileSize(tileWidth);
			currentEntity->addEventListener(ld, LDEntity::ALERTSTATE_CHANGED_EVENT);
			currentEntity->addEventListener(ld->getLiveManager(), LiveEvent::EVENT_DIE);
			scene->addChild(currentEntity);
			ld->lds.push_back(currentEntity);
		}
	}
	ld->getLiveManager()->setLivesEmil(anzWar);
	ld->getLiveManager()->setLivesKlaus(anzCiv);
	ld->getLiveManager()->setLivesMonster(ceil(anzWar / 3));

	this->dispatchEvent(new Event(), Event::COMPLETE_EVENT);
	
	loadMapData = false;

	killThread();
}

void Map::setScene(Scene * scene) { this->scene = scene; }

void TileInfoManager::setTileInfo() {
	TileInfo tileInfo;
	
	//nothing
	tileInfo.tileID = 0;
	tileInfo.passable = false;
	tileInfo.cost = -1;

	info.push_back(tileInfo);

	//gras
	tileInfo.tileID = 1;
	tileInfo.passable = true;
	tileInfo.cost = 3;

	info.push_back(tileInfo);

	tileInfo.tileID = 2;
	tileInfo.passable = true;
	tileInfo.cost = 3;

	info.push_back(tileInfo);

	tileInfo.tileID = 3;
	tileInfo.passable = true;
	tileInfo.cost = 3;

	info.push_back(tileInfo);

	tileInfo.tileID = 4;
	tileInfo.passable = true;
	tileInfo.cost = 3;

	info.push_back(tileInfo);

	//tileInfo.tileID = 5;
	//tileInfo.passable = true;
	//tileInfo.cost = 3;

	//info.push_back(tileInfo);

	tileInfo.tileID = 11;
	tileInfo.passable = true;
	tileInfo.cost = 3;

	info.push_back(tileInfo);

	//dirt
	tileInfo.tileID = 12;
	tileInfo.passable = true;
	tileInfo.cost = 5;
	
	info.push_back(tileInfo);
	tileInfo.tileID = 13;
	tileInfo.passable = true;
	tileInfo.cost = 6;

	info.push_back(tileInfo);
	tileInfo.tileID = 14;
	tileInfo.passable = true;
	tileInfo.cost = 5;

	info.push_back(tileInfo);

	tileInfo.tileID = 21;
	tileInfo.passable = true;
	tileInfo.cost = 3;

	info.push_back(tileInfo);

	tileInfo.tileID = 22;
	tileInfo.passable = true;
	tileInfo.cost = 5;

	info.push_back(tileInfo);

	tileInfo.tileID = 23;
	tileInfo.passable = true;
	tileInfo.cost = 10;

	info.push_back(tileInfo);

	tileInfo.tileID = 24;
	tileInfo.passable = true;
	tileInfo.cost = 5;

	info.push_back(tileInfo);

	//tileInfo.tileID = 25;
	//tileInfo.passable = true;
	//tileInfo.cost = 3;

	//info.push_back(tileInfo);

	tileInfo.tileID = 31;
	tileInfo.passable = true;
	tileInfo.cost = 3;

	info.push_back(tileInfo);

	tileInfo.tileID = 32;
	tileInfo.passable = true;
	tileInfo.cost = 5;

	info.push_back(tileInfo);

	tileInfo.tileID = 33;
	tileInfo.passable = true;
	tileInfo.cost = 6;

	info.push_back(tileInfo);

	tileInfo.tileID = 34;
	tileInfo.passable = true;
	tileInfo.cost = 5;

	info.push_back(tileInfo);

	//tileInfo.tileID = 35;
	//tileInfo.passable = true;
	//tileInfo.cost = 3;

	//info.push_back(tileInfo);

	//house1
	tileInfo.tileID = 62;
	tileInfo.passable = false;
	tileInfo.cost = -1;

	info.push_back(tileInfo);

	//house terrain
	tileInfo.tileID = 17;
	tileInfo.passable = false;
	tileInfo.cost = -1;

	info.push_back(tileInfo);

	tileInfo.tileID = 18;
	tileInfo.passable = false;
	tileInfo.cost = -1;

	info.push_back(tileInfo);

	tileInfo.tileID = 19;
	tileInfo.passable = false;
	tileInfo.cost = -1;

	info.push_back(tileInfo);

	tileInfo.tileID = 27;
	tileInfo.passable = false;
	tileInfo.cost = -1;

	info.push_back(tileInfo);

	tileInfo.tileID = 28;
	tileInfo.passable = false;
	tileInfo.cost = -1;

	info.push_back(tileInfo);

	tileInfo.tileID = 29;
	tileInfo.passable = false;
	tileInfo.cost = -1;

	info.push_back(tileInfo);

	tileInfo.tileID = 37;
	tileInfo.passable = false;
	tileInfo.cost = -1;

	info.push_back(tileInfo);

	tileInfo.tileID = 38;
	tileInfo.passable = false;
	tileInfo.cost = -1;

	info.push_back(tileInfo);

	tileInfo.tileID = 39;
	tileInfo.passable = false;
	tileInfo.cost = -1;

	info.push_back(tileInfo);
}

TileInfo TileInfoManager::getTileInfo(int tileID) {
	for (int i = 0; i < info.size(); i++) {
		if (info[i].tileID == tileID) {
			return info[i];
		}
	}
	return TileInfo();
}
