#pragma once
#include <Polycode.h>

using namespace Polycode;
class Intro {
public:
	Intro(Core *core);
	~Intro();

	bool Update();

private:
	Core *core;

	Scene* introScene;

	Number timeShowing;
	Number timeToShow;
	Number fadeTime;
	Number timeFading;

	int idx;
	bool showing;
	bool coming;

	SceneLabel *currentText;
	vector<SceneLabel*> texts;

	Number brightness;
};

