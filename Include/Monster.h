#pragma once
#include <Polycode.h>
#include "LDJAM33.h"
#include "Node.h"

using namespace Polycode;

class HitEvent : public Event {
public:
	HitEvent(Vector2 pos, Vector2 view) : pos(pos), view(view){ }

	Vector2 pos;
	Vector2 view;

	static const int HIT = 0;
};

class Monster :	public Entity {
public:
	Monster(Core *core);
	~Monster();
	
	void fixedUpdate();
	//void Update();
	void handleEvent(Event* e);

	void canMove();
	void findPath(Vector2 to);

	void setTileSize(int s);
	void setNodes(NodeNet* nod);

	static const int STATE_STAYING = 0;
	static const int STATE_MOVING = 1;
	static const int STATE_RUNNING = 2;
	static const int STATE_HITTING = 3;

private:
	Core* core;

	int tileSize;

	NodeNet* nodes;
	CoreInput* input;

	stack<Node*> currentPath;
	bool workingThroughPath;

	SpriteSet* spriteSet;
	SceneSprite* currentSprite;

	Number moveSpeed;
	int state;
	int lastState;
	Number timeInState;
	int reactionTime;

	void goToPos(Vector2 target);
	Vector2 lastDir;

	bool canMoveRight;
	bool canMoveLeft;
	bool canMoveUp;
	bool canMoveDown;
};

