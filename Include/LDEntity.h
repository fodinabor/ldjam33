#pragma once
#include <Polycode.h>
#include "Node.h"
#include "LDJAM33.h"

using namespace Polycode;

class AlertEvent : public Event {
public: 
	//AlertEvent(int state) { alertState = state; eventType = "AlertEvent"; }
	AlertEvent(int state, Vector2 pos) { alertState = state; lastPos = pos; eventType = "AlertEvent"; }
	~AlertEvent() { }

	int alertState;
	Vector2 lastPos;
};

class LDEntity :
	public Entity {
public:
	LDEntity(Core *core, SceneSprite *sprite, int type);
	~LDEntity();

	void fixedUpdate();
	void handleEvent(Event* e);

	void findPath(Vector2 to);
	void goToPos(Vector2 target);
	void canMove();

	void setState();

	void setWatchingFor(Entity * ent);
	void checkForWatching();

	void setTileSize(int s);
	void alertByRunning(Vector2 lastPos);

	static const int STATE_STAYING = 0;
	static const int STATE_MOVING = 1;
	static const int STATE_RUNNING = 2;
	static const int STATE_HITTING = 3;

	static const int ALERTSTATE_CHANGED_EVENT = Event::EVENTBASE_NONPOLYCODE + 0;
	static const int ALERTSTATE_RUNNING_EVENT = Event::EVENTBASE_NONPOLYCODE + 0;

	static const int ALERTSTATE_UNALERTED = 0;
	static const int ALERTSTATE_ALERTED = 1;
	static const int ALERTSTATE_SEEKING = 2;
	static const int ALERTSTATE_HUNTING = 3;
	static const int ALERTSTATE_RUNNING = 4;

	static const int TYPE_WARRIOR = 0;
	static const int TYPE_CIVILIAN = 1;

protected:
	Core* core;

	NodeNet* nodes;
	Node* currentNode;

	SceneSprite* currentSprite;
	int type;

	Entity* watchingFor;
	int viewDist;
	int alertState;
	Number timeInState;
	Number reactionSpeed;
	Number visionAngle;
	Vector2 lastViewDir;

	stack<Node*> currentPath;
	bool workingThroughPath;
	Vector2 lastKnownPos;

	int moveSpeed;
	int state;
	int lastState;

	int tileSize;

	bool canMoveRight;
	bool canMoveLeft;
	bool canMoveUp;
	bool canMoveDown;

	Number timeSinceHit;
	Number timePossibleHit;
};

