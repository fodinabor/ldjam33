#pragma once
#include <Polycode.h>
#include <polycode/modules/ui/PolycodeUI.h>

using namespace Polycode;

class LiveEvent : public Event {
public:
	LiveEvent(int who) : who(who) { eventType = "LiveEvent"; }
	~LiveEvent() { }

	int who;

	static const int EVENT_DIE = 0;

	static const int WHO_MONSTER = 0;
	static const int WHO_EMIL = 1;
	static const int WHO_KLAUS = 2;
};

class LiveManager : public UIElement {
public:
	LiveManager(Core *core, Scene *s);
	~LiveManager();

	void killKlaus();
	void killEmil();
	void killMonster();

	void setLivesKlaus(int l);
	void setLivesEmil(int l);
	void setLivesMonster(int l);

	int getLivesKlaus();
	int getLivesEmil();
	int getLivesMonster();

	void handleEvent(Event *e);

protected:
	Scene* main;

	SpriteSet* kE;
	SpriteSet* m;

	vector<SceneSprite*> livesK;
	vector<SceneSprite*> livesE;
	vector<SceneSprite*> livesM;

	int livesKlaus;
	int livesEmil;
	int livesMonster;
};

