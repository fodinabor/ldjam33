#pragma once
#include <Polycode.h>
#include <queue>
#include <time.h>

using std::stack;
using namespace Polycode;

class Node {
public:
	Node(Vector2 posNode, Vector2 mapPos) : pos(posNode), mapPos(mapPos), cost(0), distanceFromGoal(0), cameFrom(NULL) { }
	~Node();

	bool hasConnectionToNode(Node* node);
	int getConnectionIndex(Node* node);
	Node* getRandomConnectedNode();
	
	Vector2 pos;
	Vector2 mapPos;

	bool visited;
	float cost;
	float distanceFromGoal;
	Node *cameFrom;
	std::vector<Node*> connected;
	std::vector<Number> costs;
};

class NodeNet {
public:
	NodeNet() { srand(time(NULL)); };
	~NodeNet() { }

	SceneMesh * showConnections(Core* core, bool val);
	bool getShowConnections();

	Node *findNearestNode(Vector2 pos);
	stack<Node*> findPath(Node* fromNode, Node* toNode);

	Node* addNode(Vector2 pos, Vector2 mapPos);
	void addEdge(Node* node1, Node* node2, Number cost);

private:
	std::vector<Node*> nodes;
	SceneMesh* cons;
	bool showingConns;

	bool compareNodes(const Node* lhs, const Node* rhs) const {
		bool dist = lhs->cost * lhs->distanceFromGoal > rhs->distanceFromGoal * rhs->cost;
		return  dist;
	}
};

struct DereferenceCompareNode {
	bool operator()(const Node* lhs, const Node* rhs) const {
		bool dist = lhs->cost * lhs->distanceFromGoal > rhs->distanceFromGoal * rhs->cost;
		return dist;
	}
};