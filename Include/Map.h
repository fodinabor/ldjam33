#pragma once
#include "Node.h"

class LDJAM33;

struct TileInfo {
	int tileID;
	bool passable;
	int cost;
};

class TileInfoManager {
public:
	TileInfoManager() { setTileInfo(); }
	~TileInfoManager() { }

	void setTileInfo();

	TileInfo getTileInfo(int tileID);

private:
	vector<TileInfo> info;
};

class Map : public Threaded {
public:
	Map(Core *core, String mapDataFile, Scene* scene);
	~Map();

	NodeNet* getNodeNet() const;
	int getMapSize() const;

	Vector2 worldToTileCoordinates(Vector2 world);
	int getTileSize() const;
	int getTileCost(Vector2 tilePos);
	bool canPassTile(Vector2 tilePos);

	SceneMesh* ground;

	void updateThread();

	void setScene(Scene* scene);
private:
	//Object data;
	Scene* scene;
	NodeNet* nodes;
	TileInfoManager* tileInfo;
	LDJAM33* ld;

	SpriteSet* sprites;

	int sizeX;
	int sizeY;
	int** tileData;

	int tileWidth;
	int tileHeight;

	String mapDataFile;

	bool loadMapData;
};

