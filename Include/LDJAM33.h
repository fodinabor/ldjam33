#pragma once

#include "polycode/view/win32/PolycodeView.h"
#include "Polycode.h"
#include "polycode/modules/ui/PolycodeUI.h"

#include "Monster.h"
#include "Map.h"
#include "Intro.h"
#include "LiveManager.h"
#include "LDEntity.h"

using namespace Polycode;

class Monster;
class LDEntity;

class LDJAM33 : public EventHandler {
public:
	LDJAM33(PolycodeView *view);
	~LDJAM33();

	static LDJAM33* getInstance();
    
    bool Update();
	void handleEvent(Event* e);

	Map* getMap();
	Monster* getMonster();
	LiveManager* getLiveManager();
	//vector<LDEntity*> getLds();

	static const int SHOW_INTRO = 0;
	static const int SHOW_MENU = 1;
	static const int SHOW_GAME = 2;
	static const int SHOW_WON = 3;
	static const int SHOW_LOOSE = 4;
	//static const int SHOW_LOADING = 5;

	vector<LDEntity*> lds;
private:
    Core *core;
	Scene *scene;
	Scene *ui;

	static LDJAM33* overrideInstance;

	int state;
	bool mapReady;
	int dotCounter;
	Number dotDelay;
	Number showTime;
	Number alertTime;

	SceneLabel* loading;
	SceneLabel* won;
	SceneLabel* lost;
	bool wonLost;

	Map* map;
	Intro* intro;
	LiveManager* liveManager;

	Sound* introMusic;
	Sound* fightMusic;
	Sound* normalMusic;

	Monster* monster;

	UIElement *menu;
	UILabel *startGame;
	UILabel *closeGame;
	UILabel *options;
	UILabel *controls;
};

LDJAM33* ldJam();